import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as request from "request-promise-native";
import {Http} from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-sessoes',
  templateUrl: 'sessoes.html'
})
export class SessoesPage {

  public table = "<table style='width: 100%'>";  
  public tableHTML; 

  constructor(public navCtrl: NavController, private http:Http, private sanitizer: DomSanitizer) {

  	let promise = new Promise((resolve, reject) => {
      let apiURL = "http://iochallenge1-200820.appspot.com/listarSessoes";
      this.http.get(apiURL)
        .toPromise()
        .then(
          res => { // Success
            let re = /\"/gi;
            var result = res.json().data; 
            for (let entry of result) {
              this.table = this.table + "<tr><td><strong>Disciplina</td>"; 
              this.table = this.table + "<td style='text-align: left; width: 75%'>" + JSON.stringify(entry['disciplina']).replace(re, '') + "</td></tr>";
              this.table = this.table + "<tr><td><strong>Tipo</td>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['tipo']).replace(re, '') + "</td></tr>";
              this.table = this.table + "<tr><td><strong>Início</td>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['data_inicio']).replace(re, '').replace("T", " ").replace(".000Z", "") + "</td></tr>";
              this.table = this.table + "<tr><td><strong>Fim</td>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['data_fim']).replace(re, '').replace("T", " ").replace(".000Z", "") + "</td></tr>";
              this.table = this.table + "<tr><td><strong>Curso</td>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['curso']).replace(re, '') + "</td></tr>";
              this.table = this.table + "<tr><td><strong>Docente</td>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['docente']).replace(re, '') + "</td></tr>"; 
              this.table = this.table + "<tr><td><strong><span style='color: white'>-----</span></td></tr>"; 
            }
            this.table = this.table + "</table> "; 
            this.tableHTML = this.sanitizer.bypassSecurityTrustHtml(this.table); 
            resolve();
          }
        );
    });
  }
}
