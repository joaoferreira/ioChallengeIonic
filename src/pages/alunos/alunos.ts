import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as request from "request-promise-native";
import {Http} from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'page-alunos',
  templateUrl: 'alunos.html'
})
export class AlunosPage {

  public table = "<table style='width: 100%'><tr><th style='text-align: left'>Nome</th><th style='text-align: left'>Número</th><th style='text-align: left''>Curso</th></tr>";  
  public tableHTML; 

  constructor(public navCtrl: NavController, private http:Http, private sanitizer: DomSanitizer) {

    let promise = new Promise((resolve, reject) => {
      let apiURL = "http://iochallenge1-200820.appspot.com/listarAlunos";
      this.http.get(apiURL)
        .toPromise()
        .then(
          res => { // Success
            let re = /\"/gi;
            var result = res.json().data; 
            for (let entry of result) {
              this.table = this.table + "<tr>"; 
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['nome']).replace(re, '') + "</td>";
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['numero_aluno']).replace(re, '') + "</td>";
              this.table = this.table + "<td style='text-align: left'>" + JSON.stringify(entry['curso']).replace(re, '') + "</td>";
              this.table = this.table + "</tr>"; 
            }
            this.table = this.table + "</table> "; 
            this.tableHTML = this.sanitizer.bypassSecurityTrustHtml(this.table); 
            resolve();
          }
        );
    });
  }
}
