import { Component } from '@angular/core';

import { SessoesPage } from '../sessoes/sessoes';
import { AlunosPage } from '../alunos/alunos';
import { HomePage } from '../home/home';
import { PresencasPage } from '../presencas/presencas';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SessoesPage;
  tab3Root = AlunosPage;
  tab4Root = PresencasPage;

  constructor() {

  }
}
